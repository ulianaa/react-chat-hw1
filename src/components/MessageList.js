/* eslint-disable no-useless-constructor */
import React from 'react'
import Message from './Message'
import '../styles/MessageList.css'
import MyMessage from './MyMessage';
import DateFunctions from '../helpers/date'

export default class MessageList extends React.Component {
    constructor(props) {
        super(props);
    }

    compareDates(i) {
        const date = new DateFunctions();
        const data = this.props.data;
        if (data[i+1]) {
            const firstDate = new Date(data[i].created_at);
            const secondDate = new Date(data[i+1].created_at);
            if (!date.isEqual(firstDate, secondDate)) 
            return (
                <div className="date-separator">
                    <div className="date-separator-line"></div>
                    <div className="date-separator-text">
                        {date.getMonthAndDay(secondDate)}
                    </div>
                </div>
            );
            return null;
        } 
        return null;
    }

    render() {
        const props = this.props;
        const data = this.props.data;
        return (
            <div>
                {data.map((message, i) => {
                    if (message.user === 'me') return (
                        <div key={i}>
                            <MyMessage
                                data={data[i]}
                                deleteMessage={props.deleteMessage}
                                editMessage={props.editMessage}
                            />
                            {this.compareDates(i)}
                        </div>
                    );
                    return (
                        <div key={i}>
                            <Message 
                                data={data[i]}
                            />
                            {this.compareDates(i)}
                        </div>
                        );
                    })
                }
            </div>
        );
    }
}


  

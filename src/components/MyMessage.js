import React from 'react'
import '../styles/Message.css'

export default class MyMessage extends React.Component {
    constructor(props) {
        super(props);
        this.state = { isEditing: false, value: '' };
        this.onDelete = this.onDelete.bind(this);
        this.onEdit = this.onEdit.bind(this);
        this.onChange = this.onChange.bind(this);
        this.onSave = this.onSave.bind(this);
    }

    onDelete() {
        this.props.deleteMessage(this.props.data);
    }

    onEdit() {
        this.setState({ 
            isEditing: !this.state.isEditing,
            value: this.props.data.message 
        });
    }

    onChange(e) {
        this.setState({ value: e.target.value });
    }

    onSave() {
        this.props.editMessage(this.props.data, this.state.value);
        this.setState({ isEditing: false });
    }

    render() {
        const data = this.props.data;
        if (this.state.isEditing) return (
            <div className="my-message-container">
                <div className='message-text'>
                <textarea  value={this.state.value} onChange={this.onChange}></textarea>
                <button type='button' onClick={this.onSave}>save</button>
                </div>
                <div className='message-date'>{data.created_at}</div>
                <i className='fa fa-heart'></i>
                <i className='fa fa-times' onClick={this.onDelete}></i>
                <i className='fa fa-pencil' onClick={this.onEdit}></i>
            </div>
        );
        return (
            <div className="my-message-container">
                <div className='message-text'>{data.message}</div>
                <div className='message-date'>{data.created_at}</div>
                <i className='fa fa-heart'></i>
                <i className='fa fa-times' onClick={this.onDelete}></i>
                <i className='fa fa-pencil' onClick={this.onEdit}></i>
            </div>
        );
    }
}
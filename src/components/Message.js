import React from 'react'
import '../styles/Message.css'

export default class Message extends React.Component {
    constructor(props) {
        super(props);
        this.state = { isLiked: false };
        this.onClick = this.onClick.bind(this);
    }

    onClick() {
        this.setState({ isLiked: !this.state.isLiked });
    }

    render() {
        const data = this.props.data;
        const className = this.state.isLiked ? 'fa fa-heart is-liked' : 'fa fa-heart';
        return (
            <div className="message-container">
                <img src={data.avatar} alt=''/>
                <div className="message-text">{data.message}</div>
                <div className="message-date">{data.created_at}</div>
                <i className={className} onClick={this.onClick}></i>
            </div>
        )
    }
    
}
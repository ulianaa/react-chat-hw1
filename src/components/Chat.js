import React from 'react';
import Header from './Header'
import MessageList from './MessageList'
import MessageInput from './MessageInput';
import '../styles/chat.css'


export default class Chat extends React.Component {
    constructor(props) {
        super(props);
        this.state = { loading: true };
        this.sendMessage = this.sendMessage.bind(this);
        this.deleteMessage = this.deleteMessage.bind(this);
        this.editMessage = this.editMessage.bind(this);
    }

    componentDidMount() {
        fetch('https://api.myjson.com/bins/1hiqin', { method: 'GET' })
        .then(response => 
            response.ok ? response.json() : Promise.reject(Error('Failed to load')))
        .then(data => { this.setState({data, loading: false})})
        .catch(error => {
            throw error;
        });
    }

    getUsersAmount() {
        const data = this.state.data;
        const usersList = data.map(message => message.user);
        return new Set(usersList).size;
    }

    sendMessage(message) {
        const data = this.state.data;
        data.push(message);
        this.setState({ data });
    }

    deleteMessage(message) {
        const data = this.state.data;
        const index = data.indexOf(message);
        data.splice(index, 1);
        this.setState({ data });
    }

    editMessage(message, text) {
        const data = this.state.data;
        const index = data.indexOf(message);
        data[index].message = text;
        this.setState({ data });
    }

    render() {   
        const state = this.state;
        if (state.loading) return (<div className='spinner'></div>);

        const data = this.state.data;     
        const messagesAmount = this.state.data.length;
        console.log(this.state.data);
        return (<div>
                    <Header 
                        usersAmount={this.getUsersAmount()}
                        messagesAmount={messagesAmount}
                        lastMessage={data[messagesAmount - 1].created_at}
                    />
                    <MessageList 
                        data={data}
                        deleteMessage={this.deleteMessage}
                        editMessage={this.editMessage}
                    />
                    <MessageInput 
                        sendMessage={this.sendMessage}/>
                </div>
        );
    }

}









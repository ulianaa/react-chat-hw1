import React from 'react';
import Chat from './components/Chat'
import ErrorBoundary from './errors/Error'

function App() {
  return (
    <div className="App">
      <ErrorBoundary>
        <Chat />
      </ErrorBoundary>
    </div>
  );
}

export default App;
